{
    "velecName": "v21i1-all",
    "velecVersion": "v2_1_i1",
    "frequency": 100,
    "pulseWidths": [
        30,
        30,
        30,
        30,
        30,
        30
    ],
    "globalPulseWidth": 150,
    "useGlobalPulseWidth": true,
    "subSensationThresholds": [
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
    ],
    "sensationThresholds": [
        3.4155609607696535,
        2.2848732471466066,
        2.2575907707214357,
        2.0198898315429689,
        2.056898832321167,
        2.120532274246216
    ],
    "optimalValues": [
        7.631943225860596,
        4.944393634796143,
        4.9811906814575199,
        4.759854316711426,
        4.98590087890625,
        3.7218682765960695
    ],
    "discomfortThresholds": [
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
    ],
    "subSensationThresholdsPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "sensationThresholdsPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "optimalValuesPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "discomfortThresholdsPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "optimalFactor": 0
}{
    "velecName": "v21i1-all",
    "velecVersion": "v2_1_i1",
    "frequency": 100,
    "pulseWidths": [
        30,
        30,
        30,
        30,
        30,
        30
    ],
    "globalPulseWidth": 259,
    "useGlobalPulseWidth": true,
    "subSensationThresholds": [
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
    ],
    "sensationThresholds": [
        5.0851216316223148,
        2.565201997756958,
        2.2575907707214357,
        2.340487480163574,
        3.3790283203125,
        3.2183923721313478
    ],
    "optimalValues": [
        9.0,
        5.210478782653809,
        4.9811906814575199,
        5.244715690612793,
        6.052398681640625,
        4.885014057159424
    ],
    "discomfortThresholds": [
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
    ],
    "subSensationThresholdsPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "sensationThresholdsPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "optimalValuesPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "discomfortThresholdsPW": [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "optimalFactor": 0
}